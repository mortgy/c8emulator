#pragma once

#include "Window.h"

class CMainWindow : public CWindow
{
public:
	CMainWindow(HINSTANCE hInstance = NULL);
	bool Create(int nWidth, int nHeight);

#ifdef WIN32
	static BOOL CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

	BOOL OnInitDialog(HWND hWnd);
	BOOL OnSize(int nWidth, int nHeight);
	BOOL OnCommand(WORD wControl);
	BOOL OnClose();

	void OnRomStart();
	void OnRomPause();
	void OnRomClose();
#endif

	// Restore after video engine has been detached
	void Restore();

	~CMainWindow();

	static CMainWindow * s_window;

protected:
	HMENU m_hMenu, m_hFileMenu;
};

#define sMainWindow (*CMainWindow::s_window)