#pragma once

class CVirtualMachine;

// Base class for VM components (CPU, video, audio, input, ROM) 
class CVMComponent
{
public:
	CVMComponent(CVirtualMachine * pVM) : m_pVM(pVM) {}
	virtual bool Initialise() { return true; }
	virtual ~CVMComponent() {}

protected:
	CVirtualMachine * m_pVM;
};