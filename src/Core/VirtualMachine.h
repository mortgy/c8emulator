#pragma once

#include <Core/VMComponent.h>
#include <Core/CPU/CPU.h>
#include <Core/Video/Video.h>
#include <Core/Audio/Audio.h>
#include <Core/Input/Input.h>
#include <Core/ROM/Rom.h>

/* Setup a virtual machine suited to each piece of hardware */
class CVirtualMachine
{
public:
	INLINE CPU		* GetCPU()   { return m_pCPU;   }
	INLINE CVideo	* GetVideo() { return m_pVideo; }
	INLINE CAudio	* GetAudio() { return m_pAudio; }
	INLINE CInput	* GetInput() { return m_pInput; }
	INLINE CRom		* GetROM()   { return m_pRom; }

	static CVirtualMachine * Create(RomType romType);

	CVirtualMachine();
	virtual void SetupEnvironment() = 0;
	virtual bool Initialise();
	virtual void Pause();
	virtual void Resume();
	virtual void End();
	virtual ~CVirtualMachine() {}

protected:
	CPU		* m_pCPU;
	CVideo	* m_pVideo;
	CAudio	* m_pAudio;
	CInput	* m_pInput;
	CRom	* m_pRom;
};

// Not sure of a better method for handling access to derived pointers from a 
// stored base class pointer, so we'll define accessors in each derived instance
// where necessary, which will override CVirtualMachine's.
#define DEFINE_DERIVED_ACCESSOR(clsName, methodName, varName) \
	INLINE clsName * methodName() { return reinterpret_cast<clsName *>(varName); }

#define DEFINE_VM_ACCESSORS(cpuName) \
	DEFINE_DERIVED_ACCESSOR(   CPU_ ## cpuName, GetCPU,		m_pCPU); \
	DEFINE_DERIVED_ACCESSOR(CVideo_ ## cpuName, GetVideo,	m_pVideo); \
	DEFINE_DERIVED_ACCESSOR(CAudio_ ## cpuName, GetAudio,	m_pAudio); \
	DEFINE_DERIVED_ACCESSOR(CInput_ ## cpuName, GetInput,	m_pInput); \
	DEFINE_DERIVED_ACCESSOR(  CRom_ ## cpuName, GetROM,		m_pRom)

#define DEFINE_VM_ACCESSOR(cpuName) \
	DEFINE_DERIVED_ACCESSOR(   CVM_ ## cpuName, GetVM,		m_pVM)