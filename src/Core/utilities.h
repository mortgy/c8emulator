#pragma once

#define SAFE_DELETE(p) \
	if (p != NULL) \
	{ \
		delete p; \
		p = NULL; \
	}

#define SAFE_RELEASE(p) \
	if (p != NULL) \
	{ \
		p->Release(); \
		p = NULL; \
	}

INLINE void STRTOLOWER(tstring& str)
{
	for(size_t i = 0; i < str.length(); ++i)
		str[i] = (TCHAR)tolower(str[i]);
};

INLINE void STRTOUPPER(tstring& str)
{
	for(size_t i = 0; i < str.length(); ++i)
		str[i] = (TCHAR)toupper(str[i]);
};