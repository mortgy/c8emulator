#include "stdafx.h"

CEmulator * CEmulator::s_instance = NULL;

CEmulator::CEmulator(HINSTANCE hInstance /*= NULL*/) 
	: m_pVM(NULL), m_bEmulating(false), m_bEndEmulation(false)
{
	if (s_instance == NULL)
	{
		s_instance = this;
		srand((uint32)time(NULL));
		rand(); rand(); rand(); /* ensure we start on a truly random value, first few hits are in sequence */
	}

	if (hInstance == NULL)
		m_hInstance = GetModuleHandle(NULL);
}

void CEmulator::ShowError(TCHAR * szMessage, TCHAR * szCaption, uint32 uType)
{
	MessageBox(GetWindowHwnd(), szMessage, szCaption, uType);
}

int CEmulator::Run()
{
	// TO-DO: Implement fullscreen
/*
	if (!GetPrimaryMonitorResolution(defaultWidth, defaultHeight))
	{
		ShowError(_T("Could not obtain primary monitor's resolution."), _T("Error"), MB_ICONERROR);
		return -1;
	}
*/
	if (!m_window.Create(CHIP8_WIDTH * 10, CHIP8_HEIGHT * 10))
	{
		ShowError(_T("Could not create window."), _T("Error"), MB_ICONERROR);
		return -1;
	}

	MSG msg = {0};
	while (WM_QUIT != msg.message)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE) > 0)
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		if (isEmulating())
		{
			if (m_bEndEmulation)
			{
				EndEmulation();
				continue;
			}
			
			GetInput()->UpdateKeyStates();
			GetVideo()->Render();
		}

		Sleep(100);
	}

	return msg.wParam;
}

bool CEmulator::GetPrimaryMonitorResolution(uint16 & width, uint16 & height)
{
	width  = GetSystemMetrics(SM_CXSCREEN);
	height = GetSystemMetrics(SM_CYSCREEN);

	return (width != 0 && height != 0);
}

RomType CEmulator::AttemptIdentifyRomByExtension(tstring & strFilename)
{
	// Find the last "." in the filename, to indicate where the file extension starts.
	tstring::size_type pos = strFilename.find_last_of('.');

	// No "." was found, so this filename has no extension.
	if (pos == tstring::npos)
		return RomType::RomNoExtension;

	// Now pull up all characters in the string from the . onwards (i.e. ".c8")
	tstring ext = strFilename.substr(pos);

	// If there's only one, it's a ".", so we'll classify that as not having an extension.
	if (ext.length() == 1)
		return RomType::RomNoExtension;

	// Knowing that there's at least 2 characters, let's push it forward to skip the "." in the extension
	ext = ext.substr(1);

	// and make it uppercase so we only need to check one case.
	STRTOUPPER(ext);

	// Chip8 extension(s)
	if (ext == _T("C8")
		|| ext == _T("CH8"))
		return RomType::RomChip8;

	return RomType::RomUnknown;
}

bool CEmulator::LoadROM(tstring & strFilename)
{
	RomType romType;

	// If we're currently emulating, stop.
	EndEmulation();

	// Attempt to identify the ROM type so we can do something with it.
	romType = AttemptIdentifyRomByExtension(strFilename);

	// ROM has an extension, but it is unknown.
	if (romType == RomType::RomUnknown)
	{
		sEmu.ShowError(_T("Unknown ROM type. This needs to be handled."), _T("Error"), MB_ICONERROR);
		return false;
	}

	// This ROM has no file extension. A lot of formats behave like this, 
	// but since we only handle Chip8 here, we'll assume it's Chip8.
	if (romType == RomType::RomNoExtension)
	{
		sEmu.ShowError(_T("Warning: this ROM does not have an extension. Assuming it's Chip-8."), _T("Warning"), MB_ICONEXCLAMATION);
		romType = RomType::RomChip8;
	}

	// Spawn a virtual machine that will handle the ROM.
	m_pVM = CVirtualMachine::Create(romType);
	if (GetVM() == NULL)
	{
		sEmu.ShowError(_T("Could not create a virtual machine for this ROM type."), _T("Error"), MB_ICONERROR);
		return false;
	}

	if (!GetROM()->Load(strFilename))
	{
		SAFE_DELETE(m_pVM);
		return false;
	}

	if (!GetVM()->Initialise())
	{
		SAFE_DELETE(m_pVM);
		return false;
	}

	// With the CPU prepared, let's create a dedicated thread for our emulation environment, and set it up.
	GetCPU()->Start();
	m_bEmulating = true;

	m_window.OnRomStart();
	return true;
}

void CEmulator::PauseEmulation()
{
	if (!isEmulating())
		return;

	GetVM()->Pause();
	m_window.OnRomPause();
}

void CEmulator::ResumeEmulation()
{
	if (!isEmulating())
		return;

	GetVM()->Resume();
	m_window.OnRomStart();
}

void CEmulator::EndEmulation()
{
	if (!isEmulating())
		return;

	m_bEmulating = m_bEndEmulation = false;

	if (GetVM())
	{
		GetVM()->End();
		SAFE_DELETE(m_pVM);
	}

	m_window.Resize(CHIP8_WIDTH * 10, CHIP8_HEIGHT * 10);
	m_window.OnRomClose();
}

/*  Ends emulation from within a virtual machine, to avoid deadlocks. */
void CEmulator::EndEmulationFromVM()
{
	m_bEndEmulation = true;
}

CEmulator::~CEmulator()
{
	EndEmulation();
}