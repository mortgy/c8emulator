#pragma once

#include "VirtualMachine.h"

class CEmulator
{
public:
	CEmulator(HINSTANCE hInstance = NULL);

	INLINE HINSTANCE GetAppInstance()	{ return m_hInstance; }
	INLINE HWND GetWindowHwnd()			{ return GetWindow().GetHwnd(); }
	INLINE CMainWindow & GetWindow()	{ return m_window; }
	INLINE bool isEmulating()			{ return m_bEmulating && GetVideo() != NULL; }

	INLINE CVirtualMachine * GetVM()	{ return m_pVM; } 

	// Shortcuts
	INLINE CPU		* GetCPU()			{ return GetVM()->GetCPU(); } 
	INLINE CVideo	* GetVideo()		{ return GetVM()->GetVideo(); } 
	INLINE CAudio	* GetAudio()		{ return GetVM()->GetAudio(); } 
	INLINE CInput	* GetInput()		{ return GetVM()->GetInput(); } 
	INLINE CRom		* GetROM()			{ return GetVM()->GetROM(); } 

	void ShowError(TCHAR * szMessage, TCHAR * szCaption = _T("Error"), uint32 uType = MB_ICONERROR);

	int Run();

	bool GetPrimaryMonitorResolution(uint16 & width, uint16 & height);

	RomType AttemptIdentifyRomByExtension(tstring & strFilename);
	bool LoadROM(tstring & strFilename);

	void SetupEnvironment();

	void PauseEmulation();
	void ResumeEmulation();

	void EndEmulation();
	void EndEmulationFromVM();

	~CEmulator();

protected:
	HINSTANCE			m_hInstance;
	CMainWindow			m_window;
	CVirtualMachine	*	m_pVM;

	bool				m_bEmulating;		/* emulation state */
	bool				m_bEndEmulation;	/* if set, forces emulation to end from within the rendering thread */

public:
	static CEmulator * s_instance;
};

#define sEmu (*CEmulator::s_instance)