#include "stdafx.h"

#pragma comment(lib, "dinput8.lib")
#pragma comment(lib, "dxguid.lib")

CInput_Chip8::CInput_Chip8(CVM_Chip8 * pVM) 
	: CInput(pVM), m_lpDirectInput(NULL), m_lpDIKeyboard(NULL)
{
	#define MAP_KEY(vmKey, diKey) \
		m_diKeyMap[diKey] = vmKey

	MAP_KEY(C8K_1, DIK_1); MAP_KEY(C8K_2, DIK_2); MAP_KEY(C8K_3, DIK_3); MAP_KEY(C8K_4, DIK_4); 
	MAP_KEY(C8K_Q, DIK_Q); MAP_KEY(C8K_W, DIK_W); MAP_KEY(C8K_E, DIK_E); MAP_KEY(C8K_R, DIK_R);
	MAP_KEY(C8K_A, DIK_A); MAP_KEY(C8K_S, DIK_S); MAP_KEY(C8K_D, DIK_D); MAP_KEY(C8K_F, DIK_F);
	MAP_KEY(C8K_Z, DIK_Z); MAP_KEY(C8K_X, DIK_X); MAP_KEY(C8K_C, DIK_C); MAP_KEY(C8K_V, DIK_V);

	memset(&VMKeyUpdateTimes, 0, sizeof(VMKeyUpdateTimes));
}

bool CInput_Chip8::Initialise()
{
	if (FAILED(DirectInput8Create(sEmu.GetAppInstance(), DIRECTINPUT_VERSION, IID_IDirectInput8, (LPVOID *)&m_lpDirectInput, NULL)))
	{
		sEmu.ShowError(_T("Unable to initialise DirectInput."), _T("Error"), MB_ICONERROR);
		return false;
	}

	if (!InitialiseKeyboard())
		return false;

	return true;
}

bool CInput_Chip8::InitialiseKeyboard()
{
	if (FAILED(m_lpDirectInput->CreateDevice(GUID_SysKeyboard, &m_lpDIKeyboard, NULL)))
	{
		sEmu.ShowError(_T("Could not initialise keyboard: CreateDevice() failed."), _T("Error"), MB_ICONERROR);
		return false;
	}

	if (FAILED(m_lpDIKeyboard->SetDataFormat(&c_dfDIKeyboard)))
	{
		sEmu.ShowError(_T("Could not initialise keyboard: SetDataFormat() failed."), _T("Error"), MB_ICONERROR);
		return false;
	}

	if (FAILED(m_lpDIKeyboard->SetCooperativeLevel(sEmu.GetWindowHwnd(), DISCL_BACKGROUND | DISCL_NONEXCLUSIVE)))
	{
		sEmu.ShowError(_T("Could not initialise keyboard: SetCooperativeLevel() failed."), _T("Error"), MB_ICONERROR);
		return false;
	}

	if (FAILED(m_lpDIKeyboard->Acquire()))
	{
		sEmu.ShowError(_T("Could not initialise keyboard: Acquire() failed."), _T("Error"), MB_ICONERROR);
		return false;
	}

	return true;
}

void CInput_Chip8::UpdateKeyStates()
{
	if (m_lpDIKeyboard == NULL)
		return;

	if (FAILED(m_lpDIKeyboard->GetDeviceState(sizeof(DIKeyStates), &DIKeyStates)))
	{
		// Assume the device was lost
		m_lpDIKeyboard->Acquire();

		if (FAILED(m_lpDIKeyboard->GetDeviceState(sizeof(DIKeyStates), &DIKeyStates)))
			return;
	}

	uint32 tickCount = GetTickCount();
	for (DIToVMMap::iterator itr = m_diKeyMap.begin(); itr != m_diKeyMap.end(); itr++)
	{
		uint8 oldState = VMKeyPressed[itr->second], newState = DI_KEY_DOWN(DIKeyStates, itr->first);
		if (oldState != newState
			&& (tickCount - VMKeyUpdateTimes[itr->second]) >= INPUT_LOCK_DELAY)
		{
			VMKeyPressed[itr->second] = newState;
			VMKeyUpdateTimes[itr->second] = tickCount;
		}
	}
}

Chip8Key CInput_Chip8::WaitForKeyPress()
{
	uint8 * p = (uint8 *)memchr(&VMKeyPressed, 1, sizeof(VMKeyPressed));
	if (p == NULL)
		return Chip8Key::C8K_NONE;

	return (Chip8Key)*p;
}

CInput_Chip8::~CInput_Chip8()
{
	if (m_lpDIKeyboard != NULL)
	{
		m_lpDIKeyboard->Unacquire();
		m_lpDIKeyboard->Release();
		m_lpDIKeyboard = NULL;
	}

	SAFE_RELEASE(m_lpDirectInput);
}
