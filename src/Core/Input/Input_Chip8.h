#pragma once

// Locks key states for the specified time to give 
// the VM a chance to read the state before it's reset
#define INPUT_LOCK_DELAY 250 /* ms */

#define DIRECTINPUT_VERSION 0x0800
#include <dinput.h>

/**
 * Define the default Chip-8 keys
 * Actual mappings are configurable, so names
 * only exist for reference.
 **/
enum Chip8Key
{
	C8K_1 = 1,  C8K_2 = 2,  C8K_3 = 3,  C8K_4 = 12,
	C8K_Q = 4,  C8K_W = 5,  C8K_E = 6,  C8K_R = 13,
	C8K_A = 7,  C8K_S = 8,  C8K_D = 9,  C8K_F = 14,
	C8K_Z = 10, C8K_X = 0,  C8K_C = 11, C8K_V = 15,

	C8K_NONE = -1
};

typedef uint8 DIKey;

// Detect if a DirectInput key is pressed
#define DI_KEY_DOWN(states, key) (states[key] & 0x80)

class CInput_Chip8 : public CInput
{
public:
	DEFINE_VM_ACCESSOR(Chip8);

	CInput_Chip8(CVM_Chip8 * pVM);
	bool Initialise();
	bool InitialiseKeyboard();
	void UpdateKeyStates();
	Chip8Key WaitForKeyPress();
	INLINE bool isVMKeyPressed(uint8 vmKey) { return VMKeyPressed[vmKey] != 0; }
	~CInput_Chip8();

protected:
	uint8	VMKeyPressed[CHIP8_KEYS];
	uint32	VMKeyUpdateTimes[CHIP8_KEYS];
	uint8	DIKeyStates[256];

	IDirectInput8 * m_lpDirectInput; /* DI specific */
	IDirectInputDevice8 * m_lpDIKeyboard;

	typedef std::map<DIKey, Chip8Key> DIToVMMap;
	DIToVMMap m_diKeyMap;
};