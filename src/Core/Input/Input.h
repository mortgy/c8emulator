#pragma once

/* May not be needed */

class CInput : public CVMComponent
{
public:
	CInput(CVirtualMachine * pVM) : CVMComponent(pVM) {}
	virtual void UpdateKeyStates() = 0;
	~CInput() {}
};