#include "stdafx.h"

CVirtualMachine::CVirtualMachine() : 
	m_pCPU(NULL), m_pVideo(NULL), m_pAudio(NULL), 
	m_pInput(NULL), m_pRom(NULL)
{
}

CVirtualMachine * CVirtualMachine::Create(RomType romType)
{
	CVirtualMachine * pResult = NULL;

	switch (romType)
	{
	case RomType::RomChip8:
		pResult = new CVM_Chip8();
		break;
	}

	if (pResult != NULL)
		pResult->SetupEnvironment();

	return pResult;
}

bool CVirtualMachine::Initialise()
{
	// Initialise CPU
	if (!GetCPU()->Initialise())
	{
		sEmu.ShowError(_T("Could not initialise the CPU."), _T("Error"), MB_ICONERROR);
		return false;
	}

	// Initialise graphics engine
	if (!GetVideo()->Initialise())
	{
		sEmu.ShowError(_T("Could not initialise the graphics engine."), _T("Error"), MB_ICONERROR);
		return false;
	}

	GetVideo()->Resize(sEmu.GetWindow().GetWidth(), sEmu.GetWindow().GetHeight());

	// Initialise input controller
	if (!GetInput()->Initialise())
	{
		sEmu.ShowError(_T("Could not initialise input controller."), _T("Error"), MB_ICONERROR);
		return false;
	}

	return true;
}

void CVirtualMachine::Pause()
{
	if (GetCPU() != NULL)
		GetCPU()->Pause();

	if (GetVideo() != NULL)
		GetVideo()->Pause();
}

void CVirtualMachine::Resume()
{
	if (GetVideo() != NULL)
		GetVideo()->Resume();

	if (GetCPU() != NULL)
		GetCPU()->Resume();
}

void CVirtualMachine::End()
{
	if (m_pCPU != NULL)
	{
		m_pCPU->Stop();
		SAFE_DELETE(m_pCPU);
	}

	SAFE_DELETE(m_pVideo);
	SAFE_DELETE(m_pAudio);
	SAFE_DELETE(m_pInput);
	SAFE_DELETE(m_pRom);
}