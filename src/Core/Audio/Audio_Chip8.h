#pragma once

class CAudio_Chip8 : public CAudio
{
public:
	DEFINE_VM_ACCESSOR(Chip8);

	CAudio_Chip8(CVM_Chip8 * pVM);
	void Beep();
	~CAudio_Chip8() {}
};
