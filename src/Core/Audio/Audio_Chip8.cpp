#include "stdafx.h"

CAudio_Chip8::CAudio_Chip8(CVM_Chip8 * pVM) 
	: CAudio(pVM)
{
}

void CAudio_Chip8::Beep()
{
	MessageBeep(-1); /* simplicity at its finest... */
}