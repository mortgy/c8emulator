#include "stdafx.h"

CVM_Chip8::CVM_Chip8()
{
}

void CVM_Chip8::SetupEnvironment()
{
	m_pCPU = new CPUInterpreter_Chip8(this);
	// m_pVideo = new CVideo_Chip8GL(this);
	m_pVideo = new CVideo_Chip8DX(this);
	m_pAudio = new CAudio_Chip8(this);
	m_pInput = new CInput_Chip8(this);
	m_pRom = new CRom_Chip8(this);
}

void CVM_Chip8::SetEmulationMode(Chip8Mode emulationMode)
{
	// Emulation mode's already in this mode, no need to do anything.
	if (GetCPU()->Mode == emulationMode)
		return;

	GetCPU()->Mode = emulationMode;

	switch (emulationMode)
	{
	case ModeChip8:
		GetVideo()->AdjustScale(CHIP8_WIDTH, CHIP8_HEIGHT);
		break;

	case ModeSChip:
		GetVideo()->AdjustScale(SCHIP_WIDTH, SCHIP_HEIGHT);
		break;

	case ModeMChip:
		break;
	}
}

CVM_Chip8::~CVM_Chip8()
{
}