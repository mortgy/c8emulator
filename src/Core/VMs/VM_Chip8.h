#pragma once

class CVM_Chip8;

/**
 * NOTE: This implementation relies heavily on Cowgod's Chip-8 technical reference (http://devernay.free.fr/hacks/chip8/C8TECH10.HTM)
 * Many comments will be pulled straight from there, as reference.
 */

#define CHIP8_RAM		(4096)		/* bytes */

/*	The original implementation of the Chip-8 language used a 64x32-pixel monochrome display.
	Some other interpreters, most notably the one on the ETI 660, also had 64x48 and 64x64 modes. 
	To my knowledge, no current interpreter supports these modes. 
	More recently, Super Chip-48, an interpreter for the HP48 calculator, added a 128x64-pixel mode. 
	This mode is now supported by most of the interpreters on other platforms.
*/
// This is hardcoded to original implementation for now, should be more dynamic, though.
#define CHIP8_WIDTH		64
#define CHIP8_HEIGHT	32

#define SCHIP_WIDTH		128
#define SCHIP_HEIGHT	64

#define CHIP8_FONT_SIZE	80
#define SCHIP_FONT_SIZE 160

#define CHIP8_REGISTERS 16
#define SCHIP_REGISTERS 8

#define CHIP8_STACK		16 /* Chip-8 allows for up to 16 levels of nested subroutines. */
#define CHIP8_KEYS		16

#define CHIP8_ENTRY		0x200 /* standard entry point */
#define CHIP8_ENTRY_ETI 0x600 /* intended for ETI 660 */

#define CHIP8_OP_SIZE	sizeof(uint16)

enum Chip8Mode
{
	ModeChip8, /* Chip-8 */
	ModeSChip, /* Super Chip */
	ModeMChip  /* Mega Chip */
};

#include <Core/CPU/CPU_Chip8.h>
#include <Core/CPU/Interpreter/Interpreter_Chip8.h>

#include <Core/Video/Video_Chip8.h>
#include <Core/Video/Video_Chip8DX.h>
#include <Core/Video/Video_Chip8GL.h>

#include <Core/Audio/Audio_Chip8.h>
#include <Core/Input/Input_Chip8.h>

#include <Core/ROM/ROM_Chip8.h>

class CVM_Chip8 : public CVirtualMachine
{
public:
	DEFINE_VM_ACCESSORS(Chip8);

	CVM_Chip8();
	void SetupEnvironment();
	void SetEmulationMode(Chip8Mode emulationMode);
	~CVM_Chip8();
};