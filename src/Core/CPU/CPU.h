#pragma once

class CVirtualMachine;
class CPU : public CVMComponent
{
public:
	CPU(CVirtualMachine * pVM) 
		: CVMComponent(pVM), m_bIsProcessing(false), m_bIsPaused(false)
	{
	}

	INLINE bool isProcessing() { return m_bIsProcessing; }
	INLINE bool isPaused() { return m_bIsPaused; }

	virtual void Start() = 0;
	virtual void DecodeInstruction() = 0;
	virtual void Pause() = 0;
	virtual void Resume() = 0;
	virtual void Stop() = 0;

	virtual ~CPU() {}

protected:
	bool m_bIsProcessing;
	bool m_bIsPaused;
};