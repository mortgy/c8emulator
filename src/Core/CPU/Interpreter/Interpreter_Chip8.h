#pragma once

class CPUInterpreter_Chip8 : public CPU_Chip8
{
public:
	CPUInterpreter_Chip8(CVM_Chip8 * pVM);
	void DecodeInstruction();
	void UnhandledInstruction();
	~CPUInterpreter_Chip8() {}
};