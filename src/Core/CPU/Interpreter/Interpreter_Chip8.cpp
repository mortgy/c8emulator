#include "stdafx.h"

#define OP_X	((Opcode & 0x0F00) >> 8)
#define OP_Y	((Opcode & 0x00F0) >> 4)
#define OP_N	 (Opcode & 0x000F)
#define OP_KK	 (Opcode & 0x00FF)
#define OP_NNN	 (Opcode & 0x0FFF)

//#define PRINTF printf
#define PRINTF

CPUInterpreter_Chip8::CPUInterpreter_Chip8(CVM_Chip8 * pVM) 
	: CPU_Chip8(pVM)
{
}

void CPUInterpreter_Chip8::DecodeInstruction()
{
	/* 
		Due to the nature of the opcodes, this isn't practical to map to function pointers.
		We could do it, but we'd be adding unnecessary instructions (which doesn't matter much with Chip-8, but still...)
		Be better to just recompile it.
	*/

	// Build 16-bit opcode (Chip-8 stores in big-endian, so we'll build it in a portable way)
	Opcode = RAM[PC] << 8 | RAM[PC + 1];

	// Read the instruction, advance the counter. 
	// Nothing else uses it, aside from overwriting it, so this is safe.
	PC += CHIP8_OP_SIZE;

	switch (Opcode & 0xF000)
	{
		// SYS, CLS, RET opcodes
		case C8_SYS:
		{
			switch (Opcode & 0xF0FF)
			{
				case SCHIP_MODE_C8:
					PRINTF("Chip8 enabled\n");
					GetVM()->SetEmulationMode(ModeChip8);
					return;

				case SCHIP_MODE_SCHIP:
					PRINTF("Super Chip enabled\n");
					GetVM()->SetEmulationMode(ModeSChip);
					return;

				case SCHIP_SCROLL_R4:
				{
					PRINTF("SCROLL RIGHT 4\n");
					uint8 * VRAM = GetVM()->GetVideo()->VRAM;

					// Copy each line to the right 4 pixels
					for (int y = 0; y < SCHIP_HEIGHT; y++)
					{
						uint8 * line = &VRAM[y * SCHIP_WIDTH];
						memmove(&line[4], &line[0], SCHIP_WIDTH - 4);
						memset(&line[0], 0, 4);
					}

					GetVM()->GetVideo()->EnableRender();
					return;
				}

				case SCHIP_SCROLL_L4:
				{
					PRINTF("SCROLL LEFT 4\n");
					uint8 * VRAM = GetVM()->GetVideo()->VRAM;

					// Copy each line to the left 4 pixels
					for (int y = 0; y < SCHIP_HEIGHT; y++)
					{
						uint8 * line = &VRAM[y * SCHIP_WIDTH];
						memmove(&line[0], &line[4], SCHIP_WIDTH - 4);
						memset(&line[SCHIP_WIDTH - 4], 0, 4);
					}

					GetVM()->GetVideo()->EnableRender();
					return;
 				}

				case SCHIP_EXIT:
					PRINTF("EXIT\n");
					sEmu.EndEmulationFromVM();
					return;

				// 00E0 - CLS
				// Clear the display.
				case C8_CLS:
					PRINTF("CLS\n");
					GetVM()->GetVideo()->ClearVRAM();
					return;

				// 00EE - RET
				// Return from a subroutine.
				// The interpreter sets the program counter to the address at the top of the stack, then subtracts 1 from the stack pointer.		
				case C8_RET:
					PRINTF("RET\n");
					PC = Stack[SP--];
					return;

				default:
					if ((Opcode & 0xFFF0) == SCHIP_SCROLL_DN)
					{
						PRINTF("SCROLL DOWN %X\n", OP_N);
						uint8 * VRAM = GetVM()->GetVideo()->VRAM;
						uint8 lines = OP_N;

						// Copy each line (except for the bottom line, of course) down 1
						for (int y = SCHIP_HEIGHT - 1; y >= lines; --y)
							memcpy(&VRAM[y * SCHIP_WIDTH], &VRAM[(y - lines) * SCHIP_WIDTH], SCHIP_WIDTH);

						// Remove the first line, as it's copied to the next line.
						memset(&VRAM[0], 0, lines * SCHIP_WIDTH); 
						GetVM()->GetVideo()->EnableRender();
						return;
					}
			}
		}
		// End of SYS, CLS, RET opcodes

		// 1nnn - JP addr
		// Jump to location nnn.
		// The interpreter sets the program counter to nnn.	
		case C8_JP:
			PRINTF("JP %X\n", OP_NNN);
			PC = OP_NNN;
			return;

		// 2nnn - CALL addr
		// Call subroutine at nnn.
		// The interpreter increments the stack pointer, then puts the current PC on the top of the stack. The PC is then set to nnn.
		case C8_CALL:
			PRINTF("CALL %X\n", OP_NNN);
			Stack[++SP] = PC;
			PC = OP_NNN;
			return;

		// 3xkk - SE Vx, byte
		// Skip next instruction if Vx = kk.
		// The interpreter compares register Vx to kk, and if they are equal, increments the program counter by 2.
		case C8_SE_REG_VAL:
			PRINTF("SE V%X, %X\n", OP_X, OP_KK);
			if (V[OP_X] == OP_KK)
				PC += CHIP8_OP_SIZE; // Skip next instruction.
			return;

		// 4xkk - SNE Vx, byte
		// Skip next instruction if Vx != kk.
		// The interpreter compares register Vx to kk, and if they are not equal, increments the program counter by 2.
		case C8_SNE_REG_VAL:
			PRINTF("SNE V%X, %X\n", OP_X, OP_KK);
			if (V[OP_X] != OP_KK)
				PC += CHIP8_OP_SIZE; // Skip next instruction.
			return;

		// 5xy0 - SE Vx, Vy
		// Skip next instruction if Vx = Vy.
		// The interpreter compares register Vx to register Vy, and if they are equal, increments the program counter by 2.
		case C8_SE_REG_REG:
			PRINTF("SE V%X, V%X\n", OP_X, OP_Y);
			if (V[OP_X] == V[OP_Y])
				PC += CHIP8_OP_SIZE; // Skip next instruction.
			return;

		// 6xkk - LD Vx, byte
		// Set Vx = kk.
		// The interpreter puts the value kk into register Vx.
		case C8_LD_REG_VAL:
			PRINTF("LD V%X, %X\n", OP_X, OP_KK);
			V[OP_X] = OP_KK;
			return;

		// 7xkk - ADD Vx, byte
		// Set Vx = Vx + kk.
		// Adds the value kk to the value of register Vx, then stores the result in Vx. 
		case C8_ADD_REG_VAL:
			PRINTF("ADD V%X, %X\n", OP_X, OP_KK);
			V[OP_X] += OP_KK;
			return;

		// Miscellaneous arithmetic logic opcodes
		// LD Vx, Vy; OR Vx, Vy; AND Vx, Vy; 
		// XOR Vx, Vy; ADD Vx, Vy; SUB Vx, Vy; 
		// SHR Vx; SUBN Vx, Vy; SHL Vx;
		case C8_ALU_OPCODES:
			switch (Opcode & 0xF00F)
			{
				// 8xy0 - LD Vx, Vy
				// Set Vx = Vy.
				// Stores the value of register Vy in register Vx.
				case C8_LD_REG_REG:
					PRINTF("LD V%X, V%X\n", OP_X, OP_Y);
					V[OP_X] = V[OP_Y];
					return;

				// 8xy1 - OR Vx, Vy
				// Set Vx = Vx OR Vy.
				// Performs a bitwise OR on the values of Vx and Vy, then stores the result in Vx. 
				case C8_OR_REG_REG:
					PRINTF("OR V%X, V%X\n", OP_X, OP_Y);
					V[OP_X] |= V[OP_Y];
					return;

				// 8xy2 - AND Vx, Vy
				// Set Vx = Vx AND Vy.
				// Performs a bitwise AND on the values of Vx and Vy, then stores the result in Vx. 
				case C8_AND_REG_REG:
					PRINTF("AND V%X, V%X\n", OP_X, OP_Y);
					V[OP_X] &= V[OP_Y];
					return;

				// 8xy3 - XOR Vx, Vy
				// Set Vx = Vx XOR Vy.
				// Performs a bitwise exclusive OR on the values of Vx and Vy, then stores the result in Vx. 
				case C8_XOR_REG_REG:
					PRINTF("XOR V%X, V%X\n", OP_X, OP_Y);
					V[OP_X] ^= V[OP_Y];
					return;

				// 8xy4 - ADD Vx, Vy
				// Set Vx = Vx + Vy, set VF = carry.
				// The values of Vx and Vy are added together. If the result is greater than 8 bits (i.e., > 255) VF is set to 1, otherwise 0. 
				// Only the lowest 8 bits of the result are kept, and stored in Vx.
				case C8_ADD_REG_REG:
					PRINTF("ADD V%X, V%X\n", OP_X, OP_Y);
					V[0xF] = (V[OP_Y] > (0xFF - V[OP_X]));
					V[OP_X] += V[OP_Y];
					return;

				// 8xy5 - SUB Vx, Vy
				// Set Vx = Vx - Vy, set VF = NOT borrow.
				// If Vx > Vy, then VF is set to 1, otherwise 0. Then Vy is subtracted from Vx, and the results stored in Vx.
				case C8_SUB_REG_REG:
					PRINTF("SUB V%X, V%X (with borrow)\n", OP_X, OP_Y);
					V[0xF] = (V[OP_X] >= V[OP_Y]); // NOTE: docs say >, ROMs rely on >=
					V[OP_X] -= V[OP_Y];
					return;

				// 8xy6 - SHR Vx
				// Set Vx = Vx SHR 1.
				// If the least-significant bit of Vx is 1, then VF is set to 1, otherwise 0. Then Vx is divided by 2.
				case C8_SHR1_REG:
					PRINTF("SHR V%X\n", OP_X);
					V[0xF] = (V[OP_X] & 1);
					V[OP_X] >>= 1;
					return;

				// 8xy7 - SUBN Vx, Vy
				// Set Vx = Vy - Vx, set VF = NOT borrow.
				// If Vy > Vx, then VF is set to 1, otherwise 0. Then Vx is subtracted from Vy, and the results stored in Vx.
				case C8_SUBN_REG_REG:
					PRINTF("SUBN V%X, V%\n", OP_X, OP_Y);
					V[0xF] = (V[OP_Y] >= V[OP_X]); // NOTE: docs say >, ROMs rely on >=
					V[OP_X] = V[OP_Y] - V[OP_X];
					return;

				// 8xyE - SHL Vx
				// Set Vx = Vx SHL 1.
				// If the most-significant bit of Vx is 1, then VF is set to 1, otherwise to 0. Then Vx is multiplied by 2.
				case C8_SHL1_REG:
					PRINTF("SHL V%X\n", OP_X);
					V[0xF] = (V[OP_X] >> 7);
					V[OP_X] <<= 1;
					return;
			}
			break;
			// End of miscellaneous arithmetic logic opcodes

		// 9xy0 - SNE Vx, Vy
		// Skip next instruction if Vx != Vy.
		// The values of Vx and Vy are compared, and if they are not equal, the program counter is increased by 2.
		case C8_SNE_REG_REG:
			PRINTF("SNE V%X, V%X\n", OP_X, OP_Y);
			if (V[OP_X] != V[OP_Y])
				PC += CHIP8_OP_SIZE;
			return;

		// Annn - LD I, addr
		// Set I = nnn.
		// The value of register I is set to nnn.
		case C8_LD_I_VAL:
			PRINTF("LD I, %X\n", OP_NNN);
			I = OP_NNN;
			return;

		// Bnnn - JP V0, addr
		// Jump to location nnn + V0.
		// The program counter is set to nnn plus the value of V0.
		case C8_JP_REG0_VAL:
			PRINTF("JP V0, %X\n", OP_NNN);
			PC = OP_NNN + V[0];
			return;

		// Cxkk - RND Vx, byte
		// Set Vx = random byte AND kk.
		// The interpreter generates a random number from 0 to 255, which is then ANDed with the value kk. The results are stored in Vx. See instruction 8xy2 for more information on AND.
		case C8_RND_REG_VAL:
			PRINTF("RND V%X, V%X\n", OP_X, OP_KK);
			V[OP_X] = (rand() % 255) & OP_KK;
			return;

		// Dxyn - DRW Vx, Vy, nibble
		// Display n-byte sprite starting at memory location I at (Vx, Vy), set VF = collision.
		// The interpreter reads n bytes from memory, starting at the address stored in I. 
		// These bytes are then displayed as sprites on screen at coordinates (Vx, Vy). Sprites are XORed onto the existing screen. 
		// If this causes any pixels to be erased, VF is set to 1, otherwise it is set to 0. 
		// If the sprite is positioned so part of it is outside the coordinates of the display, it wraps around to the opposite side of the screen. 
		case C8_DRW_REG_REG_VAL:
		{
			uint16 x = V[OP_X], y = V[OP_Y]; 
			PRINTF("DRW V%X, V%X, %X\n", OP_X, OP_Y, OP_N);
			
			// default collision to 0
			V[0xF] = 0;

			Chip8Mode mode = GetVM()->GetCPU()->Mode;
			uint8 * VRAM = GetVM()->GetVideo()->VRAM;
			uint16 XScale = GetVM()->GetVideo()->XScale;
			uint16 spriteSize, lineHeight = OP_N;
			uint16 i = I;
			
			//  SCHIP-48 note: If n=0 and extended mode, show 16x16 sprite 
			if (mode == ModeChip8 || (mode == ModeSChip && lineHeight > 0))
			{
				spriteSize = 8;

				for (uint16 lineY = 0; lineY < lineHeight; lineY++)
				{
					uint16 pixel = RAM[i + lineY];
					for (uint16 lineX = 0; lineX < spriteSize; lineX++)
					{
						if ((pixel & (0x80 >> lineX)) != 0)
						{
							uint8 * p = &VRAM[x + lineX + ((y + lineY) * XScale)];
							if (*p)
								V[0xF] = 1; /* collision hit */

							*p ^= 1;
						}
					}
				}
			}
			else if (mode == ModeSChip)
			{
				lineHeight = spriteSize = 16;
				for (uint16 lineY = 0; lineY < lineHeight; lineY++)
				{
					uint16 pixel = RAM[i++ + lineY] << 8 | RAM[i++ + lineY];
					for (uint16 lineX = 0; lineX < spriteSize; lineX++)
					{
						if ((pixel & (0x80 >> lineX)) != 0)
						{
							uint8 * p = &VRAM[x + lineX + ((y + lineY) * XScale)];
							if (*p)
								V[0xF] = 1; /* collision hit */

							*p ^= 1;
						}
					}
				}
			}

			GetVM()->GetVideo()->EnableRender();
			return;
		}

		// SKP, SKNP opcodes
		case C8_INPUT_LOGIC:
			switch (Opcode & 0xF0FF)
			{
			// Ex9E - SKP Vx
			// Skip next instruction if key with the value of Vx is pressed.
			// Checks the keyboard, and if the key corresponding to the value of Vx is currently in the down position, PC is increased by 2.
			case C8_SKP_REG:
				PRINTF("SKP V%X (key pressed?)\n", OP_X);
				if (GetVM()->GetInput()->isVMKeyPressed(V[OP_X]))
					PC += CHIP8_OP_SIZE; // Skip next instruction.
				return;

			// ExA1 - SKNP Vx
			// Skip next instruction if key with the value of Vx is not pressed.
			// Checks the keyboard, and if the key corresponding to the value of Vx is currently in the up position, PC is increased by 2.
			case C8_SKNP_REG:
				PRINTF("SKNP V%X (key released?)\n", OP_X);
				if (!GetVM()->GetInput()->isVMKeyPressed(V[OP_X]))
					PC += CHIP8_OP_SIZE; // Skip next instruction.
				return;
			}
		break;
		// End of SKP, SKNP opcodes

		case C8_MEM_OPCODES:
			switch (Opcode & 0xF0FF)
			{
			// Fx07 - LD Vx, DT
			// Set Vx = delay timer value.
			// The value of DT is placed into Vx.
			case C8_LD_REG_DT:
				PRINTF("LD V%X, DT\n", OP_X);
				V[OP_X] = DT;
				return;

			// Fx0A - LD Vx, K
			// Wait for a key press, store the value of the key in Vx.
			// All execution stops until a key is pressed, then the value of that key is stored in Vx.
			case C8_LD_REG_K:
			{
				Chip8Key keyPressed = GetVM()->GetInput()->WaitForKeyPress();
				PRINTF("LD V%X, K\n", OP_X);

				if (keyPressed == Chip8Key::C8K_NONE)
				{
					PRINTF("Key not yet pressed...\n");
					PC -= CHIP8_OP_SIZE; /* go back an instruction, so next cycle we check again */
					return;
				}

				printf("Key pressed: %d\n", keyPressed);
				V[OP_X] = keyPressed;
				return;
			}

			// Fx15 - LD DT, Vx
			// Set delay timer = Vx.
			// DT is set equal to the value of Vx.
			case C8_LD_DT_REG:
				PRINTF("LD DT, V%X\n", OP_X);
				DT = V[OP_X];
				return;

			// Fx18 - LD ST, Vx
			// Set sound timer = Vx.
			// ST is set equal to the value of Vx.
			case C8_LD_ST_REG:
				PRINTF("LD ST, V%X\n", OP_X);
				ST = V[OP_X];
				return;

			// Fx1E - ADD I, Vx
			// Set I = I + Vx.
			// The values of I and Vx are added, and the results are stored in I.
			// VF is set to 1 when range overflow (I+VX>0xFFF), and 0 when there isn't.
			case C8_ADD_I_REG:
				PRINTF("ADD I, V%X\n", OP_X);
				V[0xF] = (I + V[OP_X] > 0xFFF);
				I += V[OP_X];
				return;

			// Fx29 - LD I, Vx (5-byte font sprite)
			// Set I = location of sprite for digit Vx.
			// The value of I is set to the location for the hexadecimal sprite corresponding to the value of Vx.
			case C8_LD_I_REG:
				PRINTF("LD I, V%X\n", OP_X);
				I = V[OP_X] * 5; /* characters are represented by a 4x5 font */
				return;

			// Fx30 - LD10 I, Vx (10-byte font sprite)
			// Set I = location of sprite for digit Vx
			// The value of I is set to the location for the hexadecimal sprite corresponding to the value of Vx.
			case SCHIP_LD_I_REG:
				PRINTF("LD10 I, V%X\n", OP_X);
				I = CHIP8_FONT_SIZE + (V[OP_X] * 10);
				return;

			// Fx33 - LD B, Vx
			// Store BCD representation of Vx in memory locations I, I+1, and I+2.
			// The interpreter takes the decimal value of Vx, and places the hundreds digit in memory at location in I, the tens digit at location I+1, and the ones digit at location I+2.
			case C8_LD_B_REG:
				PRINTF("LD B, V%X\n", OP_X);
				RAM[I + 0] = (V[OP_X] / 100);
				RAM[I + 1] = (V[OP_X] / 10) % 10;
				RAM[I + 2] = (V[OP_X] % 100) % 10;
				return;

			// Fx55 - LD [I], Vx
			// Store registers V0 through Vx in memory starting at location I.
			// The interpreter copies the values of registers V0 through Vx into memory, starting at the address in I.
			// On the original interpreter, when the operation is done, I = I + X + 1.
			case C8_LD_IP_REG:
				PRINTF("LD [I], V%X\n", OP_X);
				memcpy(&RAM[I], &V[0], OP_X + 1);
				I += OP_X + 1;
				return;

			// Fx65 - LD Vx, [I]
			// Read registers V0 through Vx from memory starting at location I.
			// The interpreter reads values from memory starting at location I into registers V0 through Vx.
			// On the original interpreter, when the operation is done, I = I + X + 1.
			case C8_LD_REG_IP:
				PRINTF("LD V%X, [I]\n", OP_X);
				memcpy(&V[0], &RAM[I], OP_X + 1);
				I += OP_X + 1;
				return;

			// Store V0..VX in RPL user flags (X <= 7)
			case SCHIP_WRITE_FLAGS:
				PRINTF("LD HP, V%X\n", OP_X);
				assert(OP_X <= 7);
				memcpy(&HP[0], &V[0], OP_X + 1);
				return;
			
			// Read V0..VX from RPL user flags (X <= 7)
			case SCHIP_READ_FLAGS:
				PRINTF("LD V%X, HP\n", OP_X);
				memcpy(&V[0], &HP[0], OP_X + 1);
				return;
			}
			break;
	}

	// If this is hit, the instruction has not been handled.
	UnhandledInstruction();
}

void CPUInterpreter_Chip8::UnhandledInstruction()
{
	char szMessage[128] = {0};
	sprintf(szMessage, "Unhandled instruction: %04X at PC=%X\n", Opcode, PC - CHIP8_OP_SIZE);
	PRINTF(szMessage);
	MessageBoxA(sEmu.GetWindowHwnd(), szMessage, "Unhandled instruction", MB_ICONERROR);
	assert(0);
}