#include "stdafx.h"

/* Define the fontset */
static const uint8 s_Chip8FontSet[CHIP8_FONT_SIZE] =
{ 
	0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
	0x20, 0x60, 0x20, 0x20, 0x70, // 1
	0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
	0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
	0x90, 0x90, 0xF0, 0x10, 0x10, // 4
	0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
	0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
	0xF0, 0x10, 0x20, 0x40, 0x40, // 7
	0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
	0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
	0xF0, 0x90, 0xF0, 0x90, 0x90, // A
	0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
	0xF0, 0x80, 0x80, 0x80, 0xF0, // C
	0xE0, 0x90, 0x90, 0x90, 0xE0, // D
	0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
	0xF0, 0x80, 0xF0, 0x80, 0x80  // F
};

static const uint8 s_SChipFontSet[SCHIP_FONT_SIZE] =
{
	0xF0, 0xF0, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0xF0, 0xF0, // 0
	0x20, 0x20, 0x60, 0x60, 0x20, 0x20, 0x20, 0x20, 0x70, 0x70, // 1
	0xF0, 0xF0, 0x10, 0x10, 0xF0, 0xF0, 0x80, 0x80, 0xF0, 0xF0, // 2
	0xF0, 0xF0, 0x10, 0x10, 0xF0, 0xF0, 0x10, 0x10, 0xF0, 0xF0, // 3
	0x90, 0x90, 0x90, 0x90, 0xF0, 0xF0, 0x10, 0x10, 0x10, 0x10, // 4
	0xF0, 0xF0, 0x80, 0x80, 0xF0, 0xF0, 0x10, 0x10, 0xF0, 0xF0, // 5
	0xF0, 0xF0, 0x80, 0x80, 0xF0, 0xF0, 0x90, 0x90, 0xF0, 0xF0, // 6
	0xF0, 0xF0, 0x10, 0x10, 0x20, 0x20, 0x40, 0x40, 0x40, 0x40, // 7
	0xF0, 0xF0, 0x90, 0x90, 0xF0, 0xF0, 0x90, 0x90, 0xF0, 0xF0, // 8
	0xF0, 0xF0, 0x90, 0x90, 0xF0, 0xF0, 0x10, 0x10, 0xF0, 0xF0, // 9
	0xF0, 0xF0, 0x90, 0x90, 0xF0, 0xF0, 0x90, 0x90, 0x90, 0x90, // A
	0xE0, 0xE0, 0x90, 0x90, 0xE0, 0xE0, 0x90, 0x90, 0xE0, 0xE0, // B
	0xF0, 0xF0, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0xF0, 0xF0, // C
	0xE0, 0xE0, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0xE0, 0xE0, // D
	0xF0, 0xF0, 0x80, 0x80, 0xF0, 0xF0, 0x80, 0x80, 0xF0, 0xF0, // E
	0xF0, 0xF0, 0x80, 0x80, 0xF0, 0xF0, 0x80, 0x80, 0x80, 0x80  // F
};

CPU_Chip8::CPU_Chip8(CVM_Chip8 * pVM)
	: CPU(pVM), m_hThread(NULL), PC(CHIP8_ENTRY)
{
	/* reset RAM */
	memset(&RAM, 0, sizeof(RAM));
}

bool CPU_Chip8::Initialise()
{
	/* reset the registers */
	memset(&V, 0, sizeof(V));
	I = 0;

	/* reset the timers */
	DT = ST = 0;

	/* reset the stack, and stack pointer register */
	SP = 0;
	memset(&Stack, 0, sizeof(Stack));

	/* load fontsets */
	memcpy(&RAM[0], &s_Chip8FontSet, CHIP8_FONT_SIZE);
	memcpy(&RAM[CHIP8_FONT_SIZE], &s_SChipFontSet, SCHIP_FONT_SIZE);

	/* default emulation mode */
	Mode = ModeChip8;

	return true;
}

void CPU_Chip8::Start()
{
	if (m_hThread != NULL)
		return;

	m_hThread = CreateThread(NULL, NULL, &CPU_Chip8::Processor, this, NULL, &m_dwThreadId);
}

DWORD WINAPI CPU_Chip8::Processor(LPVOID lpParam)
{
	const int hzLimit = 300; /* this should be variable per game, as there's no defined speed */
	const int SECOND = 1000;

	CPU_Chip8 * pCPU = static_cast<CPU_Chip8 *>(lpParam);
	DWORD tickCount, endTickCount, avg = SECOND / hzLimit;

	pCPU->m_bIsProcessing = true;

	// Spawn timer thread
	pCPU->m_hTimerThread = CreateThread(NULL, NULL, &Timers, pCPU, NULL, &pCPU->m_dwTimerThreadId);

	while (pCPU->isProcessing())
	{
		DWORD timeTaken = 0;

		/*
			Pretend each instruction will take 1 cycle.
			In reality this isn't going to be the case, but it's
			way faster than it was intended to run at as it-is...
		*/
		for (int i = 0; i < hzLimit; i++)
		{
			tickCount = GetTickCount();
			pCPU->DecodeInstruction();
			endTickCount = GetTickCount();
			timeTaken += endTickCount - tickCount;
			Sleep(avg);
		}

		if (timeTaken > SECOND)
			continue;

		avg = (SECOND - timeTaken) / hzLimit; /* not really an average because I'm lazy, and it's not worth it for Chip-8... */
	}

	// The timer thread will close when processing is false
	WaitForSingleObject(pCPU->m_hTimerThread, INFINITE);
	CloseHandle(pCPU->m_hTimerThread);
	return 0;
}

DWORD WINAPI CPU_Chip8::Timers(LPVOID lpParam)
{
	const int hzLimit = 60;
	const int SECOND = 1000;

	CPU_Chip8 * pCPU = static_cast<CPU_Chip8 *>(lpParam);
	CAudio_Chip8 * pAudio = pCPU->GetVM()->GetAudio();
	DWORD tickCount, endTickCount, avg = SECOND / hzLimit;
	
	while (pCPU->isProcessing())
	{
		DWORD timeTaken = 0;

		for (int i = 0; i < hzLimit; i++)
		{
			tickCount = GetTickCount();

			// Delay timer decrements at 60hz
			if (pCPU->DT)
				pCPU->DT--;

			// Sound timer decrements at 60hz
			// Beep is played each hit.
			if (pCPU->ST) 
			{
				pCPU->ST--;
				if (pAudio != NULL)
					pAudio->Beep();
			}

			endTickCount = GetTickCount();
			timeTaken += endTickCount - tickCount;
			Sleep(avg);
		}

		if (timeTaken > SECOND)
			continue;

		avg = (SECOND - timeTaken) / hzLimit; /* not really an average because I'm lazy, and it's not worth it for Chip-8... */
	}

	return 0;
}

void CPU_Chip8::Pause()
{
	if (isPaused())
		return;

	SuspendThread(m_hTimerThread);
	SuspendThread(m_hThread);

	m_bIsPaused = true;
}

void CPU_Chip8::Resume()
{
	if (!isPaused())
		return;

	m_bIsPaused = false;

	ResumeThread(m_hThread);
	ResumeThread(m_hTimerThread);
}

void CPU_Chip8::Stop()
{
	if (!isProcessing())
		return;

	if (isPaused())
		Resume();

	m_bIsProcessing = false;
	WaitForSingleObject(m_hThread, INFINITE);
	CloseHandle(m_hThread);
	m_hThread = NULL;
}