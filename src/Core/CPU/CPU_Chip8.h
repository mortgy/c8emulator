#pragma once

class CPU_Chip8 : public CPU
{
public:
	DEFINE_VM_ACCESSOR(Chip8);

	CPU_Chip8(CVM_Chip8 * pVM);
	bool Initialise();

	void Start();
	static DWORD WINAPI Processor(LPVOID lpParam);
	static DWORD WINAPI Timers(LPVOID lpParam);
	void Pause();
	void Resume();
	void Stop();

	/* 
		Feels so wrong, but let's lump RAM and such in with the CPU implementation...

		The Chip-8 language is capable of accessing up to 4KB (4,096 bytes) of RAM, from location 0x000 (0) to 0xFFF (4095). 
		The first 512 bytes, from 0x000 to 0x1FF, are where the original interpreter was located, and should not be used by programs.
		Most Chip-8 programs start at location 0x200 (512), but some begin at 0x600 (1536). 
		Programs beginning at 0x600 are intended for the ETI 660 computer.
	*/
	uint8 RAM [CHIP8_RAM];

	/*
		Chip-8 has 16 general purpose 8-bit registers, usually referred to as Vx, where x is a hexadecimal digit (0 through F). 
		There is also a 16-bit register called I. This register is generally used to store memory addresses, so only the 
		lowest (rightmost) 12 bits are usually used.
	*/
	uint8	V[CHIP8_REGISTERS];		/* general purpose registers */
	uint16	I;						/* index register, generally stores memory addresses */
	uint16	PC;						/* program counter / instruction pointer */

	uint8	DT;						/* delay timer, counts down at 60Hz */
	uint8	ST;						/* sound timer, counts down at 60Hz, beeps each tick */

	uint8	SP;						/* stack pointer */
	uint16	Stack[CHIP8_STACK];		/* the stack */

	Chip8Mode Mode;					/* emulation mode */

	/**
	 * SCHIP
	 **/
	uint16	HP[SCHIP_REGISTERS];	/* HP48/RPL registers */

	uint16	Opcode;					/* current opcode */

protected:
	HANDLE	m_hThread;				/* dedicated thread for handling processing */
	DWORD	m_dwThreadId;			/* thread's ID */

	HANDLE	m_hTimerThread;			/* dedicated thread for timer processing */
	DWORD	m_dwTimerThreadId;		/* timer thread's ID */
};

enum Chip8Opcodes
{
	C8_SYS				= 0x0000,
	C8_CLS				= 0x00E0,
	C8_RET				= 0x00EE,

	C8_JP				= 0x1000,
	C8_CALL				= 0x2000,
	C8_SE_REG_VAL		= 0x3000,
	C8_SNE_REG_VAL		= 0x4000,
	C8_SE_REG_REG		= 0x5000,
	C8_LD_REG_VAL		= 0x6000,
	C8_ADD_REG_VAL		= 0x7000,

	C8_ALU_OPCODES		= 0x8000, /* ALU opcode set, though not all are specified here */

	C8_LD_REG_REG		= 0x8000,
	C8_OR_REG_REG		= 0x8001,
	C8_AND_REG_REG		= 0x8002,
	C8_XOR_REG_REG		= 0x8003,
	C8_ADD_REG_REG		= 0x8004,
	C8_SUB_REG_REG		= 0x8005,
	C8_SHR1_REG			= 0x8006,
	C8_SUBN_REG_REG		= 0x8007,
	C8_SHL1_REG			= 0x800E,

	C8_SNE_REG_REG		= 0x9000,
	C8_LD_I_VAL			= 0xA000,
	C8_JP_REG0_VAL		= 0xB000,
	C8_RND_REG_VAL		= 0xC000,
	C8_DRW_REG_REG_VAL	= 0xD000,

	C8_INPUT_LOGIC		= 0xE000, /* checks on input */
	C8_SKP_REG			= 0xE09E,
	C8_SKNP_REG			= 0xE0A1,

	C8_MEM_OPCODES		= 0xF000, /* opcodes mostly relating to working with memory/timers */
	C8_LD_REG_DT		= 0xF007,
	C8_LD_REG_K			= 0xF00A,
	C8_LD_DT_REG		= 0xF015,
	C8_LD_ST_REG		= 0xF018,
	C8_ADD_I_REG		= 0xF01E,
	C8_LD_I_REG			= 0xF029,
	C8_LD_B_REG			= 0xF033,
	C8_LD_IP_REG		= 0xF055, /* name is reflective of the existing opcode mneumonic 'LD [I], Vx', despite the mneumonic misleading its intention */
	C8_LD_REG_IP		= 0xF065, /* name is reflective of the existing opcode mneumonic 'LD Vx, [I]', despite the mneumonic misleading its intention */
};

enum SChipOpcodes
{
	SCHIP_SCROLL_DN		= 0x00C0, /* Scroll display N lines down */
	SCHIP_SCROLL_R4		= 0x00FB, /* Scroll display 4 pixels right */
	SCHIP_SCROLL_L4		= 0x00FC, /* Scroll display 4 pixels left */
	SCHIP_EXIT			= 0x00FD, /* Exit CHIP interpreter */
	SCHIP_MODE_C8		= 0x00FE, /* Disable extended screen mode */
	SCHIP_MODE_SCHIP	= 0x00FF, /* Enable extended screen mode for full-screen graphics */
	SCHIP_LD_I_REG		= 0xF030, /* Point I to 10-byte font sprite for digit VX (0..9) */
	SCHIP_WRITE_FLAGS	= 0xF075, /* Store V0..VX in RPL user flags (X <= 7) */
	SCHIP_READ_FLAGS	= 0xF085, /* Read V0..VX from RPL user flags (X <= 7) */
};
