#include "stdafx.h"

/* this is an awkward design, it'd be neater to derive from DX/GL base classes */

#include <d3dx9.h>
#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")

CVideo_Chip8DX::CVideo_Chip8DX(CVM_Chip8 * pVM)
	: CVideo_Chip8(pVM), m_bResetDevice(false)
{
}

bool CVideo_Chip8DX::Initialise()
{
	m_pD3D = Direct3DCreate9(D3D_SDK_VERSION);
	if (m_pD3D == NULL)
	{
		sEmu.ShowError(_T("Could not initialise Direct3D"), _T("Error"), MB_ICONERROR);
		return false;
	}

	return SetupDevice();
}

void CVideo_Chip8DX::SetPresentationParameters()
{
	memset(&m_d3dpp, 0, sizeof(m_d3dpp));

	m_d3dpp.Windowed			= TRUE;
	m_d3dpp.SwapEffect			= D3DSWAPEFFECT_DISCARD;
	m_d3dpp.hDeviceWindow		= sEmu.GetWindowHwnd();
	m_d3dpp.BackBufferFormat	= D3DFMT_X8R8G8B8; /* not really a concern with Chip-8 being 8-bit monochrome */
	m_d3dpp.BackBufferHeight	= YScale;
	m_d3dpp.BackBufferWidth		= XScale;
	m_d3dpp.BackBufferCount		= 2;
}

bool CVideo_Chip8DX::SetupDevice()
{
	SetPresentationParameters();

	if (FAILED(m_pD3D->CreateDevice(D3DADAPTER_DEFAULT, 
		D3DDEVTYPE_HAL, /* use hardware acceleration */
		m_d3dpp.hDeviceWindow,
		D3DCREATE_SOFTWARE_VERTEXPROCESSING,
		&m_d3dpp,
		&m_pD3DDevice)))
	{
		SAFE_RELEASE(m_pD3D);
		m_pD3DDevice = NULL;
		sEmu.ShowError(_T("Unable to create display device."));
		return false;
	}

	if (FAILED(D3DXCreateFont(m_pD3DDevice, 12, 0, FW_NORMAL, 1, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, 
		DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, _T("Arial"), &m_pFont)))
	{
		SAFE_RELEASE(m_pD3DDevice);
		SAFE_RELEASE(m_pD3D);
		sEmu.ShowError(_T("Unable to create font."));
		return false;
	}

	// Create a surface for drawing on.
	if (FAILED(m_pD3DDevice->CreateOffscreenPlainSurface(XScale, YScale, D3DFMT_A8R8G8B8, D3DPOOL_DEFAULT, &m_pSurface, NULL)))
	{
		SAFE_RELEASE(m_pD3DDevice);
		SAFE_RELEASE(m_pD3D);
		sEmu.ShowError(_T("Unable to create display surface."));
		return false;
	}

	return true;
}

void CVideo_Chip8DX::Resize(int nWidth, int nHeight)
{
	if (m_pD3DDevice == NULL)
		return;

	D3DVIEWPORT9 vp;

	vp.X      = 0;
	vp.Y      = 0;
	vp.Width  = nWidth;
	vp.Height = nHeight;
	vp.MinZ   = 0.0f;
	vp.MaxZ   = 1.0f;

	m_pD3DDevice->SetViewport(&vp);
}

void CVideo_Chip8DX::ForceRender()
{
	if (m_pD3DDevice == NULL)
		return;

	FastGuard lock(m_renderLock);

	// If we need to modify the window (switching between window modes, or resolutions for instance)
	// recreate the device.
	// NOTE: Reset() should work too, but it's very fussy about use cases. Simpler to just recreate the device.
	if (m_bResetDevice)
	{
		m_bResetDevice = false;

		SAFE_RELEASE(m_pSurface);
		SAFE_RELEASE(m_pD3DDevice);

		if (!SetupDevice())
			return;
	}

	// If paused, display pause screen.
	if (isPaused())
		RenderPauseScreen();
	else
		RenderFrame();

	m_bRender = false;
}

bool CVideo_Chip8DX::LockSurface(D3DLOCKED_RECT * lpRect)
{
	// Lock the surface, so that we can draw on it.
	if (FAILED(m_pSurface->LockRect(lpRect, NULL, D3DLOCK_DISCARD)))
	{
		SAFE_RELEASE(m_pSurface);
		sEmu.ShowError(_T("Unable to lock surface for drawing."));
		m_bResetDevice = true; // attempt to reset device for next render
		return false;
	}

	return true;
}

void CVideo_Chip8DX::RenderPauseScreen()
{
	RECT rect = { 0, 0, XScale, YScale };

	m_pD3DDevice->BeginScene();
	m_pD3DDevice->Clear(0, NULL, D3DCLEAR_TARGET, 0, 0.0f, 0);
	m_pFont->DrawText(NULL, _T("Paused"), -1, &rect, DT_CENTER | DT_VCENTER | DT_NOCLIP, 0xFFFFFFFF /* white */);
	m_pD3DDevice->EndScene();
	m_pD3DDevice->Present(NULL, NULL, NULL, NULL);
}

void CVideo_Chip8DX::RenderFrame()
{
	LPDIRECT3DSURFACE9 pBackBuffer;
	D3DLOCKED_RECT lockedRect;

	if (!LockSurface(&lockedRect))
		return;

	// Convert the pixel format from 8-bit monochrome (A8, really 1-bit, or A1) to 32-bit (X8R8G8B8).
	// NOTE: We're redrawing the entire screen each time.
	for (int y = 0; y < YScale; y++)
	{
		uint32 * pData = (uint32 *)((uint8 *)lockedRect.pBits + lockedRect.Pitch * y);
		for (int x = 0; x < XScale; x++)
			pData[x] = CONVERT_A1_TO_X8R8G8B8(VRAM[y * XScale + x]);
	}

	m_pSurface->UnlockRect();

	// Retrieve the backbuffer, and copy (and stretch!) our prepared surface onto it.
	// That's all we need to do!
	m_pD3DDevice->BeginScene();
	m_pD3DDevice->GetBackBuffer(0, 0, D3DBACKBUFFER_TYPE_MONO, &pBackBuffer);
	m_pD3DDevice->StretchRect(m_pSurface, NULL, pBackBuffer, NULL, D3DTEXF_NONE);
	m_pD3DDevice->EndScene();
	m_pD3DDevice->Present(NULL, NULL, NULL, NULL);
}

void CVideo_Chip8DX::AdjustScale(int xScale, int yScale)
{
	CVideo_Chip8::AdjustScale(xScale, yScale);

	// Adjust the backbuffer size
	m_d3dpp.BackBufferHeight	= YScale;
	m_d3dpp.BackBufferWidth		= XScale;

	m_bResetDevice = true;
}

CVideo_Chip8DX::~CVideo_Chip8DX()
{
	SAFE_RELEASE(m_pSurface);
	SAFE_RELEASE(m_pFont);
	SAFE_RELEASE(m_pD3DDevice);
	SAFE_RELEASE(m_pD3D);
}
