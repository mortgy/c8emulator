#pragma once

class CVideo_Chip8 : public CVideo
{
public:
	DEFINE_VM_ACCESSOR(Chip8);

	CVideo_Chip8(CVM_Chip8 * pVM);

	INLINE void EnableRender() { m_bRender = true; }
	INLINE void SetupVRAM() 
	{
		SAFE_DELETE(VRAM);
		VRAM = new uint8[XScale * YScale]; 
		ClearVRAM();
	}
	INLINE void ClearVRAM()
	{
		if (VRAM == NULL)
			return;

		memset(VRAM, 0, XScale * YScale);
		EnableRender();
	}

	void Render() { if (m_bRender) ForceRender(); }

	virtual void ForceRender() = 0;
	virtual void AdjustScale(int xScale, int yScale);

	void Pause()  { CVideo::Pause();  m_bRender = true; } /* enable overlay */
	void Resume() { CVideo::Resume(); m_bRender = true; } /* disable overlay */

	~CVideo_Chip8();

public:
	uint8	* VRAM;
	uint16	XScale, YScale;

protected:
	bool m_bRender;
};