#pragma once

class CVideo : public CVMComponent
{
public:
	CVideo(CVirtualMachine * pVM) : CVMComponent(pVM), m_bIsPaused(false) {}

	INLINE bool isPaused() { return m_bIsPaused; }

	virtual bool Initialise()	= 0;
	virtual void Resize(int nWidth, int nHeight) = 0;

	virtual void ForceRender()	= 0;
	virtual void Render()		= 0;

	virtual void Pause() { m_bIsPaused = true; }
	virtual void Resume() { m_bIsPaused = false; }

	~CVideo() {}

protected:
	FastMutex	m_renderLock;
	bool		m_bIsPaused;
};

// Only two states: 0, and 1 (black [no colour] and white [full colour], respectively).
// Since we're dealing with monochrome, we just need to translate this to either 
// 0 (no colour, VRAM will state 0) or 0xFFFFFFFF (-1 signed, full colour, VRAM will be non-zero, but probably 1).
#define CONVERT_A1_TO_X8R8G8B8(pixel) (pixel ? -1 : 0)
