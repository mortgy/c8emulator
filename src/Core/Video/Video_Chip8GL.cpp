#include "stdafx.h"

#include <gl/GL.h>
#include <gl/GLU.h>

#pragma comment(lib, "OpenGL32.lib")
#pragma comment(lib, "GLU32.lib")

const int CVideo_Chip8GL::SurfaceDepth = 4;

CVideo_Chip8GL::CVideo_Chip8GL(CVM_Chip8 * pVM) 
	: CVideo_Chip8(pVM), m_hDC(NULL), m_hRC(NULL), m_surface(NULL)
{
}

bool CVideo_Chip8GL::Initialise()
{
	PIXELFORMATDESCRIPTOR pfd = {0};
	int iPixelFormat;

	// Acquire the device context (DC)
	// NOTE: CS_OWNDC must be set
	m_hDC = GetDC(sEmu.GetWindowHwnd());
	if (m_hDC == NULL)
	{
		sEmu.ShowError(_T("Unable to initialise OpenGL, window device context could not be obtained."), _T("Error"), MB_ICONERROR);
		return false;
	}

	pfd.nSize = sizeof(pfd);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 24;
	pfd.cDepthBits = 8;
	pfd.iLayerType = PFD_MAIN_PLANE;

	iPixelFormat = ChoosePixelFormat(m_hDC, &pfd);
	if (!SetPixelFormat(m_hDC, iPixelFormat, &pfd))
	{
		sEmu.ShowError(_T("Unable to initialise OpenGL, could not set pixel format."), _T("Error"), MB_ICONERROR);
		return false;
	}

	m_hRC = wglCreateContext(m_hDC);
	if (m_hRC == NULL)
	{
		sEmu.ShowError(_T("Unable to initialise OpenGL, unable to create OpenGL context."), _T("Error"), MB_ICONERROR);
		return false;
	}

	if (!wglMakeCurrent(m_hDC, m_hRC))
	{
		sEmu.ShowError(_T("Unable to initialise OpenGL, unable to set current OpenGL context."), _T("Error"), MB_ICONERROR);
		return false;
	}

	// Enable textures
	glEnable(GL_TEXTURE_2D);

	// Set up the texture
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP); 

	// Create a texture 
	CreateSurface();

	// Setup viewport
	Resize(sEmu.GetWindow().GetWidth(), sEmu.GetWindow().GetHeight());
	return true;
}

void CVideo_Chip8GL::CreateSurface()
{
	SAFE_DELETE(m_surface);
	m_surface = new uint8[YScale * XScale * SurfaceDepth];
	memset(m_surface, 0, YScale * XScale * SurfaceDepth);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, XScale, YScale, 0, GL_RGBA, GL_UNSIGNED_BYTE, m_surface);
}

void CVideo_Chip8GL::Resize(int nWidth, int nHeight)
{
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
	gluOrtho2D(0, nWidth, nHeight, 0);        
    glMatrixMode(GL_MODELVIEW);
	glViewport(0, 0, nWidth, nHeight);
	m_viewportWidth = nWidth;
	m_viewportHeight = nHeight;
}

void CVideo_Chip8GL::AdjustScale(int xScale, int yScale)
{
	CVideo_Chip8::AdjustScale(xScale, yScale);
	CreateSurface();
}

void CVideo_Chip8GL::ForceRender()
{
	if (!m_bRender)
		return;

	// Convert the pixel format from 8-bit monochrome (A8, really 1-bit, or A1) to 32-bit (X8R8G8B8).
	// NOTE: We're redrawing the entire screen each time.
	for (int y = 0; y < YScale; y++)
	{
		for (int x = 0; x < XScale; x++)
		{
			uint8 b = CONVERT_A1_TO_X8R8G8B8(VRAM[y * XScale + x]);
			for (int i = 0; i < SurfaceDepth; i++)
				m_surface[(y * XScale * SurfaceDepth) + (x * SurfaceDepth + i)] = b;
		}
	}

	// Clear the framebuffer
	glClear(GL_COLOR_BUFFER_BIT);

	// Build the texture
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, XScale, YScale, GL_RGBA, GL_UNSIGNED_BYTE, m_surface);

	// Draw (and scale) the texture to the viewport
	glBegin(GL_QUADS);
		glTexCoord2d(0.0, 0.0); glVertex2d(0.0, 0.0);
		glTexCoord2d(1.0, 0.0);	glVertex2d(m_viewportWidth, 0.0);
		glTexCoord2d(1.0, 1.0);	glVertex2d(m_viewportWidth, m_viewportHeight);
		glTexCoord2d(0.0, 1.0); glVertex2d(0.0, m_viewportHeight);
	glEnd();

	SwapBuffers(m_hDC);
	m_bRender = false;
}

CVideo_Chip8GL::~CVideo_Chip8GL()
{
	SAFE_DELETE(m_surface);

	if (m_hRC != NULL)
	{
		wglMakeCurrent(NULL, NULL);
		wglDeleteContext(m_hRC);
	}

	if (m_hDC != NULL)
		ReleaseDC(sEmu.GetWindowHwnd(), m_hDC);
}
