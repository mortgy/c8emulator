#include "stdafx.h"

CVideo_Chip8::CVideo_Chip8(CVM_Chip8 * pVM) :
	CVideo(pVM), m_bRender(false), 
	XScale(CHIP8_WIDTH), YScale(CHIP8_HEIGHT)
{
	SetupVRAM();
}

void CVideo_Chip8::AdjustScale(int xScale, int yScale)
{
	XScale = xScale;
	YScale = yScale;

	sEmu.GetWindow().Resize(xScale * 10, yScale * 10);

	SetupVRAM();
}

CVideo_Chip8::~CVideo_Chip8()
{
	SAFE_DELETE(VRAM);
	sEmu.GetWindow().Restore();
}
