#pragma once

struct IDirect3D9;
struct IDirect3DDevice9;
struct IDirect3DSurface9;
struct ID3DXFont;

#include <d3d9types.h>

class CPU_Chip8;
class CVideo_Chip8DX : public CVideo_Chip8
{
public:
	CVideo_Chip8DX(CVM_Chip8 * pVM);
	bool Initialise();
	void SetPresentationParameters();
	bool SetupDevice();
	void Resize(int nWidth, int nHeight);
	void ForceRender();
	bool LockSurface(D3DLOCKED_RECT * lpRect);
	void RenderPauseScreen();
	void RenderFrame();
	void AdjustScale(int xScale, int yScale);
	~CVideo_Chip8DX();

protected:
	IDirect3D9				* m_pD3D;
	IDirect3DDevice9		* m_pD3DDevice;
	IDirect3DSurface9		* m_pSurface;
	ID3DXFont				* m_pFont;
	D3DPRESENT_PARAMETERS	m_d3dpp;
	bool					m_bResetDevice;
};