#pragma once

class CVideo_Chip8GL : public CVideo_Chip8
{
public:
	CVideo_Chip8GL(CVM_Chip8 * pVM);

	bool Initialise();
	void CreateSurface();
	void Resize(int nWidth, int nHeight);
	void ForceRender();
	void AdjustScale(int xScale, int yScale);

	~CVideo_Chip8GL();

protected:
	HDC m_hDC;
	HGLRC m_hRC;
	uint8 * m_surface;
	static const int SurfaceDepth;

	int m_viewportWidth, m_viewportHeight;
};