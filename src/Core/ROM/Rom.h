#pragma once

enum RomType
{
	RomUnknown,
	RomNoExtension,
	RomChip8
};

class CRom : public CVMComponent
{
public:
	CRom(CVirtualMachine * pVM) : CVMComponent(pVM) {}

	INLINE CVirtualMachine * GetVM()   { return m_pVM; }
	INLINE tstring & GetFilename() { return m_filename; }

	virtual bool Load(tstring & strFilename) = 0;
	virtual void Unload() = 0;

protected:
	tstring m_filename;
};