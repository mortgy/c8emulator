#include "stdafx.h"

/**
 * Chip-8 ROMs are fairly simple, so there's not much for our ROM class to do.
 **/

CRom_Chip8::CRom_Chip8(CVM_Chip8 * pVM) 
	: CRom(pVM)
{
}

bool CRom_Chip8::Load(tstring & strFilename)
{
	CPU_Chip8 * pCPU = GetVM()->GetCPU();
	long lSize = 0;

	// Attempt to open the ROM
	FILE * fp = _tfopen(strFilename.c_str(), _T("rb"));
	if (fp == NULL)
	{
		sEmu.ShowError(_T("An error occurred while trying to open the ROM."), _T("Error"), MB_ICONERROR);
		return false;
	}

	m_filename = strFilename;

	// Grab the filesize
	fseek(fp, 0, SEEK_END);
	lSize = ftell(fp);

	// TO-DO: need to work out the correct entry point for this ROM
	// It could be 0x200, or 0x600 (most likely the former, though).
	// Whichever it is will impact on the RAM size check...
	const long entryPoint = CHIP8_ENTRY;

	// If the RAM's too large to be loaded at this entry point..
	if (lSize > CHIP8_RAM - entryPoint)
	{
		sEmu.ShowError(_T("This ROM cannot be opened as it is too large to fit in Chip-8 RAM."), _T("Error"), MB_ICONERROR);
		fclose(fp);
		return false;
	}

	// Read the ROM into memory
	fseek(fp, 0, SEEK_SET);
	fread(&pCPU->RAM[entryPoint], lSize, 1, fp); 

	// Mark the entry point.
	pCPU->PC = entryPoint;

	// Close the file, we don't need it anymore.
	fclose(fp);
	return true;
}
