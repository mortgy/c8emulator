#pragma once

class CRom_Chip8 : public CRom
{
public:
	DEFINE_VM_ACCESSOR(Chip8);

	CRom_Chip8(CVM_Chip8 * pVM);

	bool Load(tstring & strFilename);
	void Unload() {}
	~CRom_Chip8() {}
};