#pragma once

#define WIN32_LEAN_AND_MEAN
#define _CRT_SECURE_NO_WARNINGS

#include <Windows.h>
#include <cassert>
#include <tchar.h>
#include <commdlg.h>
#include <time.h>
#include <stdio.h>
#include <string>
#include <map>

#include "types.h"
#include "Mutex.h"

#include "MainWindow.h"

#include <Core/utilities.h>
#include <Core/Emulator.h>
#include <Core/VirtualMachine.h>
#include <Core/VMs/VM_Chip8.h>
