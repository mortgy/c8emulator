#include "stdafx.h"
#include "resource.h"

enum { IDC_BASE = 200, IDC_LOAD_ROM, IDC_PAUSE_EMULATION, IDC_END_EMULATION, IDC_EXIT };

CMainWindow * CMainWindow::s_window = NULL;

CMainWindow::CMainWindow(HINSTANCE hInstance) 
	: CWindow(hInstance), m_hMenu(NULL), m_hFileMenu(NULL)
{
}

bool CMainWindow::Create(int nWidth, int nHeight)
{
	if (s_window == NULL)
		s_window = this;

	m_width = nWidth;
	m_height = nHeight;
	m_hWnd = CreateDialog(m_hInstance, MAKEINTRESOURCE(IDD_MAIN), NULL, WndProc);

	Resize(nWidth, nHeight);
	return (m_hWnd != NULL);
}

#ifdef WIN32
BOOL CALLBACK CMainWindow::WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	// printf("hWnd=%X, uMsg=%X, wParam=%X, lParam=%X\n", hWnd, uMsg, wParam, lParam);
	switch (uMsg)
	{
	case WM_CREATE:
		return TRUE;

	case WM_INITDIALOG:
		return sMainWindow.OnInitDialog(hWnd);

	case WM_PAINT:
		if (!sEmu.isEmulating())
			return TRUE;

		// If we need to redraw, force the last frame to be rendered.
		sEmu.GetVideo()->ForceRender();
		return FALSE;

	case WM_CLOSE:
		return sMainWindow.OnClose();

	case WM_COMMAND:
		return sMainWindow.OnCommand(LOWORD(wParam));

	case WM_SIZE:
		return sMainWindow.OnSize(LOWORD(lParam), HIWORD(lParam));
	}

	return FALSE;
}

BOOL CMainWindow::OnInitDialog(HWND hWnd)
{
	m_hWnd = hWnd;

	m_hMenu = CreateMenu();
	m_hFileMenu = CreatePopupMenu();

	AppendMenu(m_hFileMenu, MF_STRING, IDC_LOAD_ROM, _T("Load ROM"));
	AppendMenu(m_hFileMenu, MF_STRING | MF_DISABLED | MF_GRAYED, IDC_PAUSE_EMULATION, _T("Pause emulation"));
	AppendMenu(m_hFileMenu, MF_STRING | MF_DISABLED | MF_GRAYED, IDC_END_EMULATION, _T("End emulation"));
	AppendMenu(m_hFileMenu, MF_STRING, IDC_EXIT, _T("Exit"));
	AppendMenu(m_hMenu,		MF_STRING | MF_POPUP, (UINT) m_hFileMenu, _T("File"));

	SetMenu(m_hWnd, m_hMenu);

	return TRUE;
}

BOOL CMainWindow::OnSize(int nWidth, int nHeight)
{
	CWindow::OnSize(nWidth, nHeight);

	if (sEmu.isEmulating())
		sEmu.GetVideo()->Resize(nWidth, nHeight);

	return TRUE;
}

BOOL CMainWindow::OnCommand(WORD wControl)
{
	switch (wControl)
	{
	case IDC_LOAD_ROM:
		{
			tstring strFilename = ShowOpenFileDialog(_T("Select a ROM"), _T("Chip8 ROMs (*.c8, *.ch8)\0*.c8;*.ch8\0"));
			if (!strFilename.empty())
				sEmu.LoadROM(strFilename);
		} break;

	case IDC_PAUSE_EMULATION:
		if (!sEmu.isEmulating())
			return TRUE;

		if (!sEmu.GetVM()->GetCPU()->isPaused())
			sEmu.PauseEmulation();
		else
			sEmu.ResumeEmulation();
		break;

	case IDC_END_EMULATION:
		sEmu.EndEmulation();
		break;

	case IDC_EXIT:
	case IDCANCEL:
		OnClose();
		break;
	}

	return TRUE;
}

void CMainWindow::OnRomStart()
{
	// Emulation started, so allow it to be ended.
	EnableMenuItem(m_hMenu, IDC_END_EMULATION, MF_ENABLED);

	// It can now be paused, also.
	EnableMenuItem(m_hMenu, IDC_PAUSE_EMULATION, MF_ENABLED);

	// Reset the text back to pause (as opposed to "resume").
	RenameMenuItem(m_hMenu, IDC_PAUSE_EMULATION, _T("Pause emulation"));
}

void CMainWindow::OnRomPause()
{
	// When a ROM is paused, rename it to state that it can be resumed.
	RenameMenuItem(m_hMenu, IDC_PAUSE_EMULATION, _T("Resume emulation"));
}

void CMainWindow::OnRomClose()
{
	// Emulation ended, so no reason to end it.
	EnableMenuItem(m_hMenu, IDC_END_EMULATION, MF_DISABLED | MF_GRAYED);

	// Nothing to pause.
	EnableMenuItem(m_hMenu, IDC_PAUSE_EMULATION, MF_DISABLED | MF_GRAYED);

	// Reset the text back to pause (as opposed to "resume").
	RenameMenuItem(m_hMenu, IDC_PAUSE_EMULATION, _T("Pause emulation"));
}

BOOL CMainWindow::OnClose()
{
	PostQuitMessage(0);
	return TRUE;
}
#endif

void CMainWindow::Restore()
{
	InvalidateRect(GetHwnd(), NULL, TRUE);
	RedrawWindow(GetHwnd(), NULL, NULL, RDW_INVALIDATE | RDW_ERASENOW | RDW_UPDATENOW);
}

CMainWindow::~CMainWindow()
{
}