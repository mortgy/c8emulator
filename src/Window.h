#pragma once

class CWindow
{
public:
	CWindow(HINSTANCE hInstance = NULL);

	INLINE HWND	  GetHwnd  () { return m_hWnd; }
	INLINE uint16 GetWidth () { return m_width; }
	INLINE uint16 GetHeight() { return m_height; }

	virtual bool Create(const TCHAR * lpszWindowName, const TCHAR * lpszClassName, WNDPROC wndProc, int nWidth, int nHeight);
	virtual BOOL OnSize(int nWidth, int nHeight);

	void Resize(int nWidth, int nHeight);
	void RenameMenuItem(HMENU hMenu, UINT uMenuID, TCHAR * tszLabel);

	tstring ShowOpenFileDialog(const TCHAR * szTitle, const TCHAR * szFilter, int nFilterIndex = 0, DWORD dwFlags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST);

	virtual ~CWindow();

protected:
	HWND		m_hWnd;
	WNDPROC		m_wndProc;
	HINSTANCE	m_hInstance;

	uint16		m_width, m_height;
};