#include "stdafx.h"

CWindow::CWindow(HINSTANCE hInstance) 
	:	m_hWnd(NULL), m_wndProc(NULL),
		m_hInstance(hInstance), 
		m_width(0), m_height(0)
{
	if (hInstance == NULL)
		m_hInstance = GetModuleHandle(NULL);
}

bool CWindow::Create(const TCHAR * lpszWindowName, const TCHAR * lpszClassName, WNDPROC wndProc, int nWidth, int nHeight)
{
	WNDCLASS wndClass = {0};

	wndClass.hInstance = m_hInstance;
	wndClass.lpfnWndProc = wndProc;
	wndClass.lpszClassName = lpszClassName;
	wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;

	if (!RegisterClass(&wndClass))
		return false;

	m_wndProc = wndProc;
	m_width = nWidth;
	m_height = nHeight;

	m_hWnd = CreateWindow(lpszClassName, lpszWindowName, WS_OVERLAPPEDWINDOW | WS_VISIBLE, 0, 0, nWidth, nHeight, NULL, NULL, m_hInstance, NULL);

	return (m_hWnd != NULL);
}

BOOL CWindow::OnSize(int nWidth, int nHeight)
{
	m_width = nWidth;
	m_height = nHeight;
	return TRUE;
}

void CWindow::Resize(int nWidth, int nHeight)
{
	OnSize(nWidth, nHeight);
	MoveWindow(m_hWnd, 0, 0, m_width, m_height, TRUE);
	
	/* resize window to intended size */
	// SetWindowPos(m_hWnd, 0, 0, 0, GetWidth(), GetHeight(), SWP_NOMOVE | SWP_NOZORDER | SWP_NOACTIVATE);
}

void CWindow::RenameMenuItem(HMENU hMenu, UINT uMenuID, TCHAR * tszLabel)
{
	MENUITEMINFO menuInfo;

	menuInfo.cbSize = sizeof(menuInfo);
	menuInfo.fMask = MIIM_STRING;
	menuInfo.dwTypeData = tszLabel;

	SetMenuItemInfo(hMenu, uMenuID, FALSE, &menuInfo);
}

tstring CWindow::ShowOpenFileDialog(const TCHAR * szTitle, const TCHAR * szFilter, int nFilterIndex /*= 0*/, DWORD dwFlags /*= OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST*/)
{
	TCHAR filename[MAX_PATH] = {0};
	tstring result;

	OPENFILENAME ofn = {0};

	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hInstance = m_hInstance;
	ofn.hwndOwner = GetHwnd();
	ofn.lpstrFile = filename;
	ofn.nMaxFile = sizeof(filename);
	ofn.lpstrFilter = szFilter;
	ofn.nFilterIndex = nFilterIndex;
	ofn.lpstrTitle = szTitle;
	ofn.Flags = dwFlags;

	if (GetOpenFileName(&ofn))
		result = filename;

	return result;
}

CWindow::~CWindow()
{
	if (m_hWnd != NULL)
		DestroyWindow(m_hWnd);
}